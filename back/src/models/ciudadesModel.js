// import sequelize
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../database');

var ciudades = sequelize.define('ciudades',{
  ciudad_id:{
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  ciudad_nom: Sequelize.STRING,
  ciudad_del: Sequelize.INTEGER
},
{
  timestamps: false,
});
 
module.exports = ciudades;