var express = require('express');
var route = express();
// import controller
const ciudadesController = require('./controllers/ciudadesController')
const sedeController = require('./controllers/sedeController')
// CIUDADES
route.get('/ciudad/index',ciudadesController.index);
route.post('/ciudad/create',ciudadesController.create);
route.delete('/ciudad/delete/:id',ciudadesController.delete);
route.get('/ciudad/get/:id',ciudadesController.get);

// SEDES
route.get('/sedes',sedeController.index);

// exprot route
module.exports = route;