const express = require('express');
const app = express();
const routes =  require('./routes')
var cors = require('cors')



//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
//app.use(express.urlencoded({ extended: true }));
//app.use(express.json());

var bodyParser = require('body-parser')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}) );

app.use(cors())

app.use('/API/',routes);

app.listen(app.get('port'),()=>{
  console.log("Start server on port "+app.get('port'))
})