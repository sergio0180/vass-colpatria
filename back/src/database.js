var Sequelize = require('sequelize');

const database = new Sequelize(
  'vass', // name database
  'root', // user database
  '', // password database
  {
    host: 'localhost',
    dialect: 'mysql' // mariadb / sqlite / postgres
  }
);

database.sync()

module.exports = database;