const ciudadesController = {}
var ciudadModel = require('../models/ciudadesModel');

ciudadesController.create = async ( req, res ) => {
  const data = req.body 
  try {

    const response = await ciudadModel.create({ 
      ciudad_id: null,
      ciudad_nom: data.ciudad_nom,
      ciudad_del:0
    })
    .then(function(data){
      const res = { success: true, data: data, message:"created successful" }
      return res;
    })
    .catch(error=>{
      const res = { success: false, error: error }
      return res;
    })
    res.json(response);

  } catch (e) {
    console.log(e);
  }
};

ciudadesController.delete = async ( req, res) =>{
  console.log('ITS WORKS');
  const { id } = req.params;
  console.log(id+" Este es el id");
  try {
    const response = await ciudadModel.update({
      ciudad_id:id,
      ciudad_del:1
    },{
      where: { ciudad_id: id}
    })
    .then(function(data){
      const res = { success: true, data: data, message:"updated successful" }
      return res;
    })
    .catch(error=>{
      const res = { success: false, error: error }
      return res;
    })
    res.json(response);

  } catch (e) {
    console.log(e);
  }
}


 ciudadesController.index =  async (req,res) => {
  try {
    const response = await ciudadModel.findAll({
      // include: [ Countries ]
      // include: [{
      //   model: Countries,
        where: { ciudad_del: 0 }
      // }]
    }).then(function(data){
      const res = { success:true, message: "Load successful", data: data}
      return res;
    })  
    .catch(error=>{
      const res = { success: false, error: error }
      return res
    })
    
    return res.json(response);

  } catch (error) {
    console.log("Error Ciudades controller");
    console.log(e);
  }
};

ciudadesController.get =  async (req,res) => {
  const { id } = req.params;
  try {
    const response = await ciudadModel.findAll({
      // include: [ Countries ]
      // include: [{
      //   model: Countries,
        where: { ciudad_del: 0,ciudad_id: id }
        // where: { ciudad_id: id }
      // }]
    }).then(function(data){
      const res = { success:true, message: "Load successful", data: data}
      return res;
    })  
    .catch(error=>{
      const res = { success: false, error: error }
      return res
    })
    
    return res.json(response);

  } catch (error) {
    console.log("Error Ciudades controller");
    console.log(e);
  }
};

module.exports = ciudadesController;
