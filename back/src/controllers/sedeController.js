const sedeController = {}

sedeController.index = (req,res) => {

  const data = {
    name: "SEDE",
    age: 20,
    city: 'London'
  }

  res.json(data);
};

sedeController.delete = (req,res) => {

  const data = {
    name: "DELETE",
    age: 20,
    city: 'London'
  }

  res.json(data);
};

module.exports = sedeController;
