import { Component, HostBinding, OnInit } from '@angular/core';

import {CiudadesService }  from '../../../services/ciudades.service';
import {ciudades} from '../../../model/ciudades/ciudades.module'
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-ciudades-form',
  templateUrl: './ciudades-form.component.html',
  styleUrls: ['./ciudades-form.component.css']
})

export class CiudadesFormComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  ciudad : ciudades = {
    ciudad_nom : '',
    ciudad_del: 0,
    ciudad_id:  0,
  }

  edit: boolean = false;
  
  constructor(private ciudadesService: CiudadesService, private router: Router, private activatedRoute: ActivatedRoute ) { }
  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.ciudadesService.getCiudad(params.id)
        .subscribe(
          res => {
            const response: any = res
            this.ciudad.ciudad_nom = response.data.ciudad_nom ;
            console.log(response.data);
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  saveCiudad() {
    
    this.ciudadesService.create(this.ciudad)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/ciudades']);

        },
        err => console.error(err)
      )
  }
  editCiudad() {
    
  }

}
