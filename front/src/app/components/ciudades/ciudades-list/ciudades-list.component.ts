import { Component, OnInit } from '@angular/core';

import {CiudadesService }  from '../../../services/ciudades.service';
import {ciudades} from '../../../model/ciudades/ciudades.module';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-ciudades-list',
  templateUrl: './ciudades-list.component.html',
  styleUrls: ['./ciudades-list.component.css']
})
export class CiudadesListComponent implements OnInit {

  constructor( private ciudadesService: CiudadesService, private router: Router, private activatedRoute: ActivatedRoute) 
  {   
    
  }

  ciudades: any = [];

  ngOnInit() {
    this.ciudadesService.getCiudades().subscribe(
      response => 
      {
        const res: any = response
          if(res.success){
          this.ciudades = res.data ;
         }
      },
      err => console.error(err)
    );
  }

  deleteCiudad(id: string) {
    console.log(id);
    this.ciudadesService.delete(id)
      .subscribe(
        res => {
          console.log(res);
          this.ngOnInit();
        },
        err => console.error(err)
      )
      

  }

}

// export const ciudadesListComponent = new CiudadesListComponent();

