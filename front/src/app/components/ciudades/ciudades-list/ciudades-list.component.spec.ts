import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CiudadesListComponent } from './ciudades-list.component';

describe('CiudadesListComponent', () => {
  let component: CiudadesListComponent;
  let fixture: ComponentFixture<CiudadesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CiudadesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CiudadesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
