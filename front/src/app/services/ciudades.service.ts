import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http' 
import {ciudades} from  '../model/ciudades/ciudades.module'
// import {obser}
@Injectable({
  providedIn: 'root'
})
export class CiudadesService {

  // API_URI = 'http://localhost:3000/'
  API_URI = 'http://localhost:3000/API';

  constructor(private http: HttpClient) { }
  
  getCiudades ()
  {
    return this.http.get('http://localhost:3000/API/ciudad/index');
  }

  getCiudad (id:string)
  {
    return this.http.get('http://localhost:3000/API/ciudad/get/'+id);
  }

  delete(id:string)
  {
    return this.http.delete('http://localhost:3000/API/ciudad/delete/'+id);
  }

  update(ciudad : ciudades)
  {
    return this.http.post('${this.API_URI}/ciudades/update/${id}',ciudad);
  }

  create(ciudad : ciudades)
  {
    return this.http.post('http://localhost:3000/API/ciudad/create',ciudad);
  }

  form(id:string)
  {
    return this.http.get('${this.API_URI}/ciudades/form');
  }

}
