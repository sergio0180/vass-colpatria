import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CiudadesListComponent } from './components/ciudades/ciudades-list/ciudades-list.component';
import { CiudadesFormComponent } from './components/ciudades/ciudades-form/ciudades-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'x',
    pathMatch: 'full'
  },
  {
    path:'ciudades',
    component:CiudadesListComponent,
  },
  {
    path:'ciudades/form',
    component:CiudadesFormComponent,
  },
  {
    path: 'ciudades/edit/:id',
    component: CiudadesFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
