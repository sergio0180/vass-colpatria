import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CiudadesListComponent } from './components/ciudades/ciudades-list/ciudades-list.component';
import { HttpClientModule} from '@angular/common/http';
import {CiudadesService} from "./services/ciudades.service"
import { HttpClient } from 'selenium-webdriver/http';
import { CiudadesFormComponent } from './components/ciudades/ciudades-form/ciudades-form.component';
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    CiudadesListComponent,
    CiudadesFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
      CiudadesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
